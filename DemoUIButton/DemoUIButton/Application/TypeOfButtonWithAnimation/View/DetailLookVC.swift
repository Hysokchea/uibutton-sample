//
//  DetailLookVC.swift
//  DemoUIButton
//
//  Created by joseph on 6/10/22.
//

//                  * * * * * * * * * * * * * * * * *
//                  * + Configuration UIButton      *
//                  *   - Plain Button              *
//                  *   - Gray Button               *
//                  *   - Filled Button             *
//                  * + State Button                *
//                  *   - isSelected                *
//                  *   - isHighlighted             *
//                  *   - isEnable                  *
//                  * * * * * * * * * * * * * * * * * By: Joseph_Crise


import UIKit

class DetailLookVC: UIViewController {
    
    @IBOutlet weak var plainButton  : UIButton!
    @IBOutlet weak var grayButton   : UIButton!
    @IBOutlet weak var tintedButton : UIButton!
    @IBOutlet weak var filledButton : UIButton!
    
    
    //declare veriable to check condition
    var isSelect :Bool = true
    
    
    
    //    @IBOutlet weak var button: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
   
  
    //Plain button
    @IBAction func plainBtn(_ sender: UIButton) {
        sender.shake{
            print("Plain button style")
        }
    }
    
    //Gray button
    @IBAction func grayBtn(_ sender: UIButton) {
        sender.press(){
            print("Gray button style")
        }
    }
    
   
    //Tinted button
    @IBAction func tintedBtn(_ sender: UIButton) {
        sender.rotates{
            print("Gray button style")
        }
    }
    
    //Filled button
    @IBAction func filledBtn(_ sender: UIButton) {
        sender.zoomIn{
            print("Gray button style")
        }
    }
}
