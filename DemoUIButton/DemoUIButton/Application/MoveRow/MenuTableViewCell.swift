//
//  MenuTableViewCell.swift
//  DemoUIButton
//
//  Created by joseph on 3/10/22.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    //Connection Outlet
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var nameProLabel     : UILabel!
    @IBOutlet weak var detailButton     : UIButton!
    @IBOutlet weak var productImage     : UIImageView!
    
    // declare boolean type veriable to check button is click or unclick
    var isCheck :Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func config(data: MenuModel){
        nameProLabel.text = data.productName
        productImage.image = UIImage(named: data.productImage ?? "")
        likeLabel.text      = data.likeNumber
    }

    // This buttonLike function use when user click on it then do something
    @IBAction func buttonLike(_ sender: Any) {
        isCheck = !isCheck
        
        // isCheck equal false thet mean user unclick on it
        if isCheck {
            detailButton.setImage(UIImage(systemName: "heart"), for: .normal)
            likeLabel.text = ""
            print("Unseleted")
            if !isCheck{
                detailButton.setImage(UIImage(systemName: "heart.fill"), for: .normal)
            }
       // isCheck equal true thet mean user click on it
        } else {
            detailButton.setImage(UIImage(systemName: "heart.fill"), for: .normal)
            likeLabel.text = "1 Like"
            print("seleted")
        }
    }
}
